import {Pokemon} from "./pokemon";

export interface IBattleProps {
    name: string;
    arena: string;
    firstPokemon: Pokemon;
    secondPokemon: Pokemon;
}

export class Battle implements IBattleProps {
    name: string;
    arena: string;
    firstPokemon: Pokemon;
    secondPokemon: Pokemon;
    fightingPokemon: Pokemon;
    waitingPokemon: Pokemon;
    round: number;
    moveCount: number;

    constructor(props: IBattleProps) {
        this.name = props.name;
        this.arena = props.arena;
        this.firstPokemon = props.firstPokemon;
        this.secondPokemon = props.secondPokemon;
        this.fightingPokemon = Pokemon.decideFirstBetween(props.firstPokemon, props.secondPokemon);
        this.waitingPokemon = this.fightingPokemon === this.firstPokemon ? this.secondPokemon : this.firstPokemon;
        this.round = 0;
        this.moveCount = 0;
    }

    async launch() {
        if(this.firstPokemon === undefined || this.secondPokemon === undefined) {
            throw new Error("At least one pokemon is missing in this battle!");
        }
        else {
            console.log(`Battle starting : ${this.firstPokemon.name} vs ${this.secondPokemon.name}`)
            console.log(`${this.firstPokemon.name} : ${this.firstPokemon.hp}HP | ${this.secondPokemon.name} : ${this.secondPokemon.hp}HP `)
            const winner = await this.fighting();
            console.log(`Battle finished : ${this.firstPokemon.name} vs ${this.secondPokemon.name}`)
            console.log(`${this.firstPokemon.name} : ${this.firstPokemon.hp}HP | ${this.secondPokemon.name} : ${this.secondPokemon.hp}HP `)
            return winner;
        }
    }

    async fighting() {
        return new Promise(resolve => {
            let battleInterval = setInterval(() => {
                console.log("================================")
                console.log(`${this.fightingPokemon.name}'s turn!`)

                const lostHp = this.fightingPokemon.attacks(this.waitingPokemon, this.fightingPokemon.pickAttackRandomly());
                console.log(`${this.waitingPokemon.name} lost ${lostHp}HP!`)
                console.log(`${this.waitingPokemon.name} is now at ${this.waitingPokemon.hp}HP!`)
                this.switchFighterPokemon();

                if(this.isOnePokemonDead()) {
                    // Stop battle
                    console.log(`Battle : ${this.firstPokemon.name} vs ${this.secondPokemon.name} is over`)
                    const winner = this.getWinner();
                    resolve(winner);
                    clearInterval(battleInterval);
                }
            }, 1000)
        });
    }


    isOnePokemonDead(): boolean {
        return this.firstPokemon.hp <= 0 || this.secondPokemon.hp <= 0;
    }

    getWinner(): Pokemon | undefined {
        if(this.firstPokemon.hp <= 0 && this.secondPokemon.hp <= 0) {
            console.log("None of the pokemon win, both died during the fight!");
            return undefined;
        }
        else {
            if(this.firstPokemon.hp <= 0 && this.secondPokemon.hp > 0) {
                console.log(`${this.secondPokemon.name} won the fight with ${this.secondPokemon.hp}HP against ${this.firstPokemon.name} (${this.firstPokemon.hp}HP)`);
                return this.secondPokemon;
            }
            if(this.firstPokemon.hp > 0 && this.secondPokemon.hp <= 0) {
                console.log(`${this.firstPokemon.name} won the fight with ${this.firstPokemon.hp}HP against ${this.secondPokemon.name} (${this.secondPokemon.hp}HP)`);
                return this.firstPokemon;
            }
        }
    }

    switchFighterPokemon() {
        const temporaryPokemon: Pokemon = this.fightingPokemon;
        this.fightingPokemon = this.waitingPokemon;
        this.waitingPokemon = temporaryPokemon;
    }
}
