export interface IMoveProps {
    name: string;
    accuracy: number;
    power: number;
    type: string;
}

export class Move implements IMoveProps{
    name: string;
    accuracy: number;
    power: number;
    type: string;

    constructor(props: IMoveProps) {
        this.name = props.name;
        this.accuracy = props.accuracy;
        this.power = props.power;
        this.type = props.type;
    }
}
