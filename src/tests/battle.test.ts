import {Pokemon, Move, Battle} from "../models";

describe("battle launch()", () => {

    const pikachuAttacks: Move[] = [
        {
            name: "éclair au chocolat",
            power: 100,
            type: "électriquo-patissier",
            accuracy: 10,
        },
        {
            name: "éclair à la pistache",
            power: 100,
            type: "électriquo-patissier",
            accuracy: 10,
        },
        {
            name: "éclair au café",
            power: 100,
            type: "électriquo-patissier",
            accuracy: 10,
        },
        {
            name: "éclair de lune",
            power: 100,
            type: "électriquo-astronomique",
            accuracy: 10,
        }
    ];
    const carapuceAttacks: Move[] = [
        {
            name: "jet d'eau au chocolat",
            power: 100,
            type: "eau-patissier",
            accuracy: 10,
        },
        {
            name: "jet d'eau à la pistache",
            power: 100,
            type: "eau-patissier",
            accuracy: 10,
        },
        {
            name: "jet d'eau au café",
            power: 100,
            type: "eau-patissier",
            accuracy: 10,
        },
        {
            name: "jet d'eau de lune",
            power: 100,
            type: "eau-stronomique",
            accuracy: 10,
        }
    ]
    const pikachu = new Pokemon({
        name: "Pikachu",
        hp: 50000,
        speed: 3,
        moves: pikachuAttacks
    });
    const carapuce = new Pokemon({
        name: "Carapuce",
        hp: 150,
        speed: 2,
        moves: carapuceAttacks
    });

    const battle = new Battle({
        name: "Battle 1",
        firstPokemon: pikachu,
        secondPokemon: carapuce,
        arena: "Bourpalette"
    });

    it('Pikachu should win since his life is 50 000 against 150HP for Carapuce', async () => {
        expect(await battle.launch()).toBe(pikachu);
    });


    it('Carapuce should win since his life is 50 000 against 150HP for Pikachu', async () => {
        carapuce.hp = 50000
        pikachu.hp = 150
        expect(await battle.launch()).toBe(carapuce);
    });

});