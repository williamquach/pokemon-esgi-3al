import {Pokemon, Move} from "../models";

describe("pokemon attacks()", () => {
    let random: (() => number) | undefined;

    beforeEach(() => {
        random = () => 0;
    })

    const pikachuAttacks: Move[] = [
        {
            name: "éclair au chocolat",
            power: 100,
            type: "électriquo-patissier",
            accuracy: 10,
        },
        {
            name: "éclair à la pistache",
            power: 100,
            type: "électriquo-patissier",
            accuracy: 10,
        },
        {
            name: "éclair au café",
            power: 100,
            type: "électriquo-patissier",
            accuracy: 10,
        },
        {
            name: "éclair de lune",
            power: 100,
            type: "électriquo-astronomique",
            accuracy: 10,
        }
    ];
    const carapuceAttacks: Move[] = [
        {
            name: "éclair au chocolat",
            power: 100,
            type: "électriquo-patissier",
            accuracy: 10,
        },
        {
            name: "éclair à la pistache",
            power: 100,
            type: "électriquo-patissier",
            accuracy: 10,
        },
        {
            name: "éclair au café",
            power: 100,
            type: "électriquo-patissier",
            accuracy: 10,
        },
        {
            name: "éclair de lune",
            power: 100,
            type: "électriquo-astronomique",
            accuracy: 10,
        }
    ]
    const pikachu = new Pokemon({
        name: "Pikachu",
        hp: 50,
        speed: 3,
        moves: pikachuAttacks
    });
    const carapuce = new Pokemon({
        name: "Carapuce",
        hp: 150,
        speed: 2,
        moves: carapuceAttacks
    });

    it('should get first pikachu move when picking index 0', () => {
        expect(pikachu.pickAttackRandomly(random)).toBe(pikachu.moves[0]);
    });

    it('should return 100 when pikachu attacks carapuce because carapuce has 150HP - 50 = 100', () => {
        const chosenAttack: Move = pikachu.pickAttackRandomly() || new Move({
            name: "attaque basique",
            accuracy: 10,
            power: 50,
            type: "zut"
        });
        expect(pikachu.attacks(carapuce, chosenAttack)).toBe(100);
    });

});