import {Pokemon} from "../models";

describe("pokemon firstPlayer()", () => {

    const pikachu = new Pokemon({
        name: "Pikachu",
        hp: 50,
        speed: 3
    });
    const carapuce = new Pokemon({
        name: "Carapuce",
        hp: 50,
        speed: 2
    });

    it('should return pikachu when pikachu is speeder than carapuce', () => {
        expect(Pokemon.decideFirstBetween(pikachu, carapuce)).toBe(pikachu);
    });

    const salameche = new Pokemon({
        name: "Salameche",
        hp: 50,
        speed: 8,
    });
    const bulbizarre = new Pokemon({
        name: "Bulbizarre",
        hp: 50,
        speed: 2
    });
    it('first pokemon to attack is Salameche because he is speeder', () => {
        expect(Pokemon.decideFirstBetween(salameche, bulbizarre)).toBe(salameche);
    });

    const tiplouf = new Pokemon({
        name: "Tiplouf",
        hp: 50,
        speed: 2
    });
    const magicarpe = new Pokemon({
        name: "Magicarpe",
        hp: 60,
        speed: 2
    });

    let random: (() => number) | undefined;

    beforeEach(() => {
        random = () => 0;
    })

    it("should select a random pokemon when both have equal speed", () => {
        expect(Pokemon.decideFirstBetween(tiplouf, magicarpe, random)).toBe(tiplouf)
    });
});